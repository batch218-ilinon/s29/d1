db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "87654321",
		email: "janedoe@mail.com"
	},
	courses: ["CSS", "Javascript", "Python"],
	department: "none"
});

// $gt/$gte operator 
	 /*
		- Allows us to find documents that have field number values greater than or equal to specified value.
		- Syntax:
			db.collectionName.find({field:{$gt/$gte: value}});
	 */

db.users.find(
	{
		age:{
			$gt: 65
		}
	}
);

db.users.find(
	{
		age:{
			$gte: 65
		}
	}
);

//--------------------------------

db.users.find(
	{
		age:{
			$lt: 65
		}
	}
);

db.users.find(
	{
		age:{
			$lte: 65
		}
	}
);


//$ne operator
/*
		- Allows us to find documents field number values not equal to a specified value.
		-Syntax:
			db.collectionName.find({field: { $ne: value}});
	 */

db.users.find(
	{
		age:{
			$ne: 82
		}
	}
);

// $in operator
/*
		- Allows us to find documents with specific match criteria one with one field using different values.
        -  Selects the documents where the value of a field equals any value in the specified array. 
		- Syntax:
			db.collectionName.find({field: {$in: [valueA, valueB]}});
	*/

db.users.find(
	{
		lastName:{
			$in: ['Hawking', 'Doe']
		}
	}
);

db.users.find(
	{
		courses:{
			$in: ['HTML', 'React']
		}
	}
);

// [SECTION] Logical Query Operators

// $or operator
/*
	- Allows us to find documents that match a single criteria from multiple provide search criteria
	- Syntax:
		db.collectionName.find({ $or: [{fieldA:valueA}, {fieldB:valueB}]});
*/

// Multiple field value pairs
db.users.find(
	{
		$or: [
			{firstName: "Neil"},
			{age: {$gt: 25}}
		]
	}
);

// $and operator
/*
	- Allows us to find documents matching multiple criteria in a single field.
	- Syntax:
		db.collectionName.find({$and: [{fieldA: valueA}, {fieldB: valueB}]});
*/

db.users.find(
	{
		$and: [
			{age: {$ne: 82}},
			{age: {$ne: 76}}
		]
	}
);

db.users.find(
	{
		$and: [
			{age: {$ne: 82}},
			{department: "Operations"}
		]
	}
);

// $or vs $and
/*
	$or - even just one is true the value will be true
	$and - all must be true
*/

// [SECTION] Field Projection
// To help with the readability of the values returned, we can include/exclude fields from the response.

// Inclusion
/*
	- Allows us to include/add specific fields only when retrieving a document.
	- The value provided is 1 to denote that the field is being included in the retrieval.
	- Syntax:
		db.collectionName.find({criteria}, {field: 1});
*/

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 0,
		lastName: 0,
		contact: 0
	}
)